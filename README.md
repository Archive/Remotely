### Remotely is no longer being developed. As an alternative, GNOME Connections can be used.

GNOME Connections is available to download from Flathub: 

<a href='https://flathub.org/apps/details/org.gnome.Connections'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

Homepage: https://wiki.gnome.org/Apps/Connections

---

# Remotely

![screenshot](https://gitlab.gnome.org/World/Remotely/raw/master/data/screenshots/1.png)

Remotely is a simple VNC viewer for the GNOME desktop environment. It supports common authentication methods. The display can be adjusted with three different modes so that the most optimal presentation is always possible, regardless of the remote display size.

## Install
Make sure you have Flatpak installed. [Get more information](http://flatpak.org/getting.html)

## Building
Remotely can be built and run with [Gnome Builder](https://wiki.gnome.org/Apps/Builder) >= 3.28.
Just clone the repo and hit the run button!

You can get Builder from [here](https://wiki.gnome.org/Apps/Builder/Downloads).

## Code Of Conduct
We follow the [GNOME Code of Conduct](/CODE_OF_CONDUCT.mdmd).
All communications in project spaces are expected to follow it.
