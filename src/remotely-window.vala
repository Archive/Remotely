/* remotely-window.vala
 *
 * Copyright (C) 2018 Felix Häcker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Vnc;
using Gtk;

public enum ZoomMode{
	FIT_WINDOW,
	BEST_FIT,
	ORIGINAL_SIZE
}

[GtkTemplate (ui = "/de/haeckerfelix/Remotely/ui/window.ui")]
public class Remotely.Window : Gtk.ApplicationWindow {

	[GtkChild] Entry connect_entry;
    [GtkChild] Stack connect_stack;

	[GtkChild] Stack vnc_stack;
	[GtkChild] Revealer connection_revealer;
    [GtkChild] CheckButton view_only_checkbutton;

    [GtkChild] Revealer notification_revealer;
	[GtkChild] Label notification_label;

	[GtkChild] Box vnc_display_box;
	[GtkChild] ScrolledWindow scrolled_window;

	[GtkChild] Entry username_entry;
	[GtkChild] Label username_label;
	[GtkChild] Entry clientname_entry;
	[GtkChild] Label clientname_label;
	[GtkChild] Entry password_entry;
	[GtkChild] Label password_label;

    private Display display;
    private ZoomMode zoom_mode = ZoomMode.BEST_FIT;
    private string host = "localhost";
    private string port = "5900";

	public Window (Gtk.Application app) {
		Object (application: app);

		// Apply stylesheet
		var provider = new CssProvider ();
		provider.load_from_resource ("/de/haeckerfelix/Remotely/ui/style.css");
        StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

		display = new Display();
		display.local_pointer = true;
		display.set_visible(true);
		vnc_display_box.add(display);

		connect_signals();
	}

    private void establish_connection(){
        connect_stack.set_visible_child_name("loading");
        display.open_host(host,port);
    }

    private void disconnect_connection(){
        display.close();
    }

	private void authenticate_connection (ValueArray authlist){
		connect_stack.set_visible_child_name("authenticate");

		username_entry.set_visible(false);
		username_label.set_visible(false);
		clientname_entry.set_visible(false);
		clientname_label.set_visible(false);
		password_entry.set_visible(false);
		password_label.set_visible(false);

		foreach (Value val in authlist.values) {
			DisplayCredential cred = (DisplayCredential)val.get_enum();
			switch(cred){
				case DisplayCredential.PASSWORD: password_entry.set_visible(true); password_label.set_visible(true); break;
				case DisplayCredential.USERNAME: username_entry.set_visible(true); username_label.set_visible(true); break;
				case DisplayCredential.CLIENTNAME: clientname_entry.set_visible(true); clientname_label.set_visible(true); break;
			}
		}
	}

    private void show_notification(string text){
		notification_label.set_label(text);
		notification_revealer.set_reveal_child(true);
	}

    private void connect_signals(){
        display.vnc_connected.connect(() => {
            connect_stack.set_visible_child_name("loading");
		});

		display.vnc_disconnected.connect(() => {
		    show_new_connection_page();
		});

		display.vnc_initialized.connect(() => {
			vnc_stack.set_visible_child_name("connection");
			connection_revealer.set_reveal_child(true);
			update_zoom_mode();

			var gtk_settings = Gtk.Settings.get_default ();
            gtk_settings.gtk_application_prefer_dark_theme = true;
		});

		display.vnc_auth_credential.connect((authlist) => {
			authenticate_connection(authlist);
		});

		display.vnc_auth_failure.connect((error) => {
			show_notification("Authentication error");
			show_new_connection_page();
		});

		display.vnc_error.connect((error) => {
			show_notification(error);
			show_new_connection_page();
		});

		display.vnc_auth_unsupported.connect(() => {
			show_notification("Authentication method is not supported");
			show_new_connection_page();
		});

		this.size_allocate.connect(() => {
			update_zoom_mode();
		});
    }

    private void show_new_connection_page(){
        vnc_stack.set_visible_child_name("new-connection");
        connect_stack.set_visible_child_name("host");
		connection_revealer.set_reveal_child(false);

		var gtk_settings = Gtk.Settings.get_default ();
        gtk_settings.gtk_application_prefer_dark_theme = false;
    }

	[GtkCallback]
	private void connect_button_clicked(){
	    if (connect_entry.get_text() == "" ||connect_entry.get_text() == null)
	        return;

		string[] connection = (connect_entry.get_text()).split(":");
		if(connection[1] == null) connection[1] = "5900";
		if(int.parse(connection[1]) < 5900) connection[1] = (int.parse(connection[1])+5900).to_string();

        host = connection[0];
        port = connection[1];

        establish_connection();
		update_zoom_mode();
	}

	[GtkCallback]
	private void disconnect_button_clicked(){
		disconnect_connection();
	}

	[GtkCallback]
	private void authenticate_button_clicked(){
		display.set_credential(DisplayCredential.PASSWORD, password_entry.get_text());
		display.set_credential(DisplayCredential.USERNAME, username_entry.get_text());
		display.set_credential(DisplayCredential.CLIENTNAME, clientname_entry.get_text());
	}

	[GtkCallback]
	private void view_best_fit_button_clicked(){
	    zoom_mode = ZoomMode.BEST_FIT;
	    update_zoom_mode();
	}

	[GtkCallback]
	private void view_fit_window_button_clicked(){
	    zoom_mode = ZoomMode.FIT_WINDOW;
	    update_zoom_mode();
	}

	[GtkCallback]
	private void view_original_size_button_clicked(){
	    zoom_mode = ZoomMode.ORIGINAL_SIZE;
	    update_zoom_mode();
	}

	[GtkCallback]
	private void view_only_button_clicked(){
		display.read_only = view_only_checkbutton.active;
	}

	[GtkCallback]
	private void ctrlaltdel_clicked(){
		uint[] keys = {0xFFE3, 0xFFE9, 0xFFFF};
		display.send_keys(keys);
	}

	[GtkCallback]
	private void notification_close_button_clicked(){
		notification_revealer.set_reveal_child(false);
	}

	private void update_zoom_mode(){
		// Set defaults
		display.expand = false;
		display.set_scaling(false);
		scrolled_window.hscrollbar_policy = PolicyType.ALWAYS;
		scrolled_window.vscrollbar_policy = PolicyType.ALWAYS;
		vnc_display_box.set_halign(Gtk.Align.FILL);
		vnc_display_box.set_valign(Gtk.Align.FILL);

		switch(zoom_mode){
			case ZoomMode.FIT_WINDOW: {
				scrolled_window.hscrollbar_policy = PolicyType.NEVER;
				scrolled_window.vscrollbar_policy = PolicyType.NEVER;

				display.expand = true;
				display.set_scaling(true);
				display.height_request = 0;
				display.width_request = 0;
				break;
			}
			case ZoomMode.BEST_FIT: {
				display.set_scaling(true);

				int new_width;
				int new_height;
				optiscale(vnc_stack.get_allocated_width(), vnc_stack.get_allocated_height(), display.width, display.height, out new_width, out new_height);
				display.width_request = new_width;
				display.height_request = new_height;

				vnc_display_box.set_halign(Gtk.Align.CENTER);
				vnc_display_box.set_valign(Gtk.Align.CENTER);

				break;
			}
			case ZoomMode.ORIGINAL_SIZE: {
				display.height_request = display.height;
				display.width_request = display.width;
				break;
			}
		}
	}

	private void optiscale(int box_width, int box_height, int image_width, int image_height, out int new_width, out int new_height){
		new_width = image_width;
		new_height = image_height;

		// first check if we need to scale width
		if (image_width > box_width) {
		    //scale width to fit
		    new_width = box_width;
		    //scale height to maintain aspect ratio
		    new_height = (new_width * image_height) / image_width;
		}

		// then check if we need to scale even with the new height
		if (new_height > box_height) {
		    //scale height to fit instead
		    new_height = box_height;
		    //scale width to maintain aspect ratio
		    new_width = (new_height * image_width) / image_height;
		}
	}
}
